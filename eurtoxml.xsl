<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:bsi="http://www.bsi.si"
                version="1.0">

 <!--
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 -->

 <xsl:template match="bsi:DtecBS" xmlns="http://euro.ctrl-alt-del.si/xmlout">

<currencyConversion>
<data>
 <amount><xsl:value-of select="document('-')//eurto/eur" /></amount>
 <toCurrency><xsl:value-of select="document('-')//eurto/currency" /></toCurrency>
</data>
<result>
 <validOn><xsl:value-of select="/bsi:DtecBS/bsi:tecajnica/@datum" /></validOn>
 <xsl:for-each select="/bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
  <xsl:if test="@oznaka=document('-')//eurto/currency">
   <accurate><xsl:value-of select="document('-')//eurto/eur * ."/></accurate>
   <rounded>
    <whole><xsl:value-of select="floor(document('-')//eurto/eur * .)"/></whole>
    <cents><xsl:value-of select="round((document('-')//eurto/eur * .) * 100) mod 100"/></cents>
   </rounded>
  </xsl:if>
 </xsl:for-each>
</result>
<author>
 <interface>http://ggg.ctrl-alt-del.si/authors/dustwolf.xml</interface>
 <data>http://www.bsi.si/_data/tecajnice/dtecbs.xml</data>
</author>
</currencyConversion>

 </xsl:template>
</xsl:stylesheet>

