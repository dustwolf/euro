<?php

 /*
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 */

$mime="text/html";
header("Content-type: ".$mime.";charset=utf-8");
passthru("curl http://www.bsi.si/_data/tecajnice/dtecbs.xml | xsltproc rates.xsl -");
?>
