<?php

 /*
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 */

$mime="text/html";
header("Content-type: ".$mime.";charset=utf-8");

//$UID=base_convert(str_replace(".","",$_SERVER['REMOTE_ADDR'].rand()),10,16);
//$tmpfile="/var/www/tmp/euro".$UID.".xml";
$data="<eurto><eur>".$_GET["eur"]."</eur><currency>".$_GET["currency"]."</currency></eurto>";
$data=escapeshellarg($data);
//$f=fopen($tmpfile,"w"); fwrite($f,$data); fclose($f);

exec("curl http://www.bsi.si/_data/tecajnice/dtecbs.xml > /var/www/tmp/eurodb.xml");
passthru("echo ".$data." | xsltproc eurto.xsl /var/www/tmp/eurodb.xml");
//unlink($tmpfile);
?>
