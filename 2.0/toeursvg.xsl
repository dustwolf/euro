<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:bsi="http://www.bsi.si"
                version="1.0">

 <!--
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 -->

<xsl:output 	method="xml" encoding="UTF-8" indent="yes"
		doctype-system="http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd" 
		doctype-public="-//W3C//DTD SVG 1.0//EN"/>

<xsl:template match="parameters" xmlns="http://www.w3.org/2000/svg">

<xsl:variable name="myCurrency" select="/parameters/currency" />
<xsl:variable name="myAmount" select="/parameters/amount" />

<svg width="100%" height="100%" version="1.1">
 <xsl:for-each select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
  <xsl:if test="@oznaka=$myCurrency">
   <text x="0" y="18" style="fill:#000000; font-size:18;" font-family="freesans"><xsl:value-of select="round(($myAmount div .) * 100) div 100"/> €</text>
  </xsl:if>
 </xsl:for-each>
</svg>

 </xsl:template>
</xsl:stylesheet>
