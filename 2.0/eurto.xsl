<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:bsi="http://www.bsi.si"
                version="1.0">

 <!--
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 -->

 <xsl:output method="xml" encoding="UTF-8" indent="yes"
             doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 
             doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"/>

 <xsl:template match="parameters" xmlns="http://www.w3.org/1999/xhtml">

<xsl:variable name="myCurrency" select="/parameters/currency" />
<xsl:variable name="myAmount" select="/parameters/amount" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="application/xml+xhtml; charset=UTF-8" http-equiv="content-type" />
  <title>Currency converter</title>
</head>
<body style="color: white; background-color: rgb(0, 0, 153);">
<div style="text-align: center;">
<h1>Currency converter</h1>
<hr/>
<br/>
<form method="get" action="eurto">
 <input name="amount" value="{$myAmount}" />
 &#8194;EUR to&#8194;
 <select name="currency">
 <xsl:for-each select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
  <xsl:choose>
   <xsl:when test="@oznaka=$myCurrency">
    <option selected="selected"><xsl:value-of select="@oznaka" /></option>
   </xsl:when>
   <xsl:otherwise>
    <option><xsl:value-of select="@oznaka" /></option>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:for-each>
 </select>
 &#8194;
 <button type="submit">convert</button>
</form>
<br/><br/>
<form method="get" action="toeur">
 <input name="amount" value="0" />
 &#8194;
 <select name="currency">
 <xsl:for-each select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
  <option><xsl:value-of select="@oznaka" /></option>
 </xsl:for-each>
 </select>
 &#8194;to EUR&#8194;
 <button type="submit">convert</button>
</form>
<br/>
<hr/>
<br/>
<xsl:for-each select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
 <xsl:if test="@oznaka=$myCurrency">
  <h1>
  <xsl:value-of select="$myAmount * ."/>&#8194;<xsl:value-of select="$myCurrency"/><br/>
   <small>
    or <xsl:value-of select="floor($myAmount * .)"/>&#8194;<xsl:value-of select="$myCurrency"/>&#8194;and 
    <xsl:value-of select="round(($myAmount * .) * 100) mod 100"/> cents 
   </small>
  </h1>
 </xsl:if>
</xsl:for-each>
<small>
 ECB reference rates on: <xsl:value-of select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/@datum" /><br/>
 <br/>
 <br/>
</small>
 <hr/>
<small>
 <small>
  Data feed provided by <a style="color:yellow" href="http://www.bsi.si">Bank of Slovenia</a>.<br/>
  Website by <a style="color:yellow" href="http://ggg.ctrl-alt-del.si/authors/dustwolf.xml">DustWolf</a>.
 </small>
</small>

</div>
</body>
</html>

 </xsl:template>
</xsl:stylesheet>

