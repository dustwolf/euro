<?php

 /*
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 */

$src=substr(escapeshellcmd(rawurldecode($_SERVER["PATH_INFO"])),1);
$src=str_replace(".","",$src); $src=str_replace("/","",$src);
if($src=="") {
 header("Location: index.php/index");
 die();
}

$src=getcwd()."/".$src.".xsl";
if(!file_exists($src)) {
 header("HTTP/1.0 404 Not Found"); 
 echo "404 Not Found";
 die();
}

header("Content-type: application/xhtml+xml;charset=utf-8");

$data='<?xml version="1.0" encoding="UTF-8"?>
       <parameters>
        <amount>'.$_GET["amount"].'</amount>
        <currency>'.$_GET["currency"].'</currency>
       </parameters>';

passthru("echo ".escapeshellarg($data)." | xsltproc ".escapeshellarg($src)." -");
?>
