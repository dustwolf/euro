<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:bsi="http://www.bsi.si"
                version="1.0">

 <!--
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 -->

 <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" />

 <xsl:template match="parameters" xmlns="http://euro.ctrl-alt-del.si/xmlout">

<xsl:variable name="myCurrency" select="/parameters/currency" />
<xsl:variable name="myAmount" select="/parameters/amount" />

<currencyConversion>
<data>
 <amount><xsl:value-of select="$myAmount" /></amount>
 <currency><xsl:value-of select="$myCurrency" /></currency>
</data>
<result>
 <validOn><xsl:value-of select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/@datum" /></validOn>
 <xsl:for-each select="document('dtecbs.xml')//bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
  <xsl:if test="@oznaka=$myCurrency">
   <accurate><xsl:value-of select="$myAmount div ."/></accurate>
   <rounded>
    <whole><xsl:value-of select="floor($myAmount div .)"/></whole>
    <cents><xsl:value-of select="round(($myAmount div .) * 100) mod 100"/></cents>
   </rounded>
  </xsl:if>
 </xsl:for-each>
</result>
<author>
 <interface>http://ggg.ctrl-alt-del.si/authors/dustwolf.xml</interface>
 <data>http://www.bsi.si/_data/tecajnice/dtecbs.xml</data>
</author>
</currencyConversion>

 </xsl:template>
</xsl:stylesheet>

