<?xml version="1.0"  encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:bsi="http://www.bsi.si"
                version="1.0">

 <!--
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 -->

 <xsl:output method="xml" encoding="UTF-8" indent="yes"
             doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 
             doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"/>

 <xsl:template match="bsi:DtecBS" xmlns="http://www.w3.org/1999/xhtml">
  <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
    <title></title>
   </head>
   <body style="color: white; background-color: rgb(0, 0, 153);"
         alink="#FFFF00" link="#FFFF00" vlink="#FFFF00">
    <div style="text-align: center;">
     <table style="width: 50%; text-align: left; margin-left: auto; margin-right: auto;"
            border="0" cellpadding="2" cellspacing="2">
      <caption>Euro exchange rates<br/><small>ECB reference rates on <xsl:value-of select="/bsi:DtecBS/bsi:tecajnica/@datum"/></small></caption>
      <tbody>
       <tr>
        <td>ID</td>
        <td>Currency</td>
        <td/>
        <td>1 € is</td>
       </tr>
       <xsl:for-each select="/bsi:DtecBS/bsi:tecajnica/bsi:tecaj">
       <tr>
        <td><xsl:value-of select="@sifra"/></td>
        <td><xsl:value-of select="@oznaka"/></td>
        <td><img alt="[flag]" src="flags/{@oznaka}.png" /></td>
        <td><xsl:value-of select="."/></td>
       </tr>
       </xsl:for-each>
      </tbody>
     </table>
     <hr/>
     <small><small>
     Data feed provided by <a href="http://www.bsi.si">Bank of Slovenia</a>.<br/>
     International flags by <a href="http://flagpedia.net">Flagpedia.net</a>.<br/>
     Website by <a href="http://ggg.ctrl-alt-del.si/authors/dustwolf.xml">DustWolf</a>.
     </small></small>
    </div>
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>
