<?php

 /*
      Copyright 2008,2009 Jure Sah

      This file is part of euro.ctrl-alt-del.si.

      euro.ctrl-alt-del.si is free software: you can redistribute it and/or 
      modify it under the terms of the GNU General Public License as published 
      by the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information 
 */

$br = strtolower($_SERVER['HTTP_USER_AGENT']); 

if(stripos("msie", $br) === False) {
 header("Location: 2.0/");
 die();
}

$mime="text/html";
header("Content-type: ".$mime.";charset=utf-8");
passthru("curl -x 127.0.0.1:3128 http://www.bsi.si/_data/tecajnice/dtecbs.xml | xsltproc calculator.xsl -");
?>
